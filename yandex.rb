# coding: utf-8
require 'curb'


class Curl::Easy
	alias :body :body_str
	def code
		header_str.scan(/[0-9]{3}/)[0]
	end

	def get_cookies
		val = []
		header_str.scan(/Set-Cookie: (.*?);/).each { |c| val << c[0] }
		val
	end

	def location
		header_str.scan(/Location: (.*?)\r/)[0][0] || ''
	end
end

class YandexAcc
	CONNECTION_ERROR = 1
	LOGIN_ERROR = 2
	CATPCHA_DETECTED = 3


	attr_accessor :login, :password, :proxy, :proxy_pw, :proxy_type
	attr_reader :session, :logged

	def initialize(login=nil, password=nil)
		@login = login
		@password = password
		@logged = false
		@session = ''
		@proxy = nil
		@proxy_pw = nil
		@proxy_type = Curl::CURLPROXY_HTTP

		# proxy types
		# CURLPROXY_HTTP
		# CURLPROXY_SOCKS4
		# CURLPROXY_SOCKS5
	end

	def test
		# request url: 'https://www.google.com', proxy: '12.167.241.76:38005', https: true
		request url: 'https://secure.internode.on.net/webtools/showmyip'
	end

	def get_direct_inner_page(keyword)
		unless @logged
			res, error = try_login
			if res.nil?
				case error
					when YandexAcc::CONNECTION_ERROR
		  				puts "NO connection to server"
		  				exit
					when YandexAcc::LOGIN_ERROR
						puts "Cant login in account"
						exit
					when YandexAcc::CATPCHA_DETECTED
		  				puts "Captcha detected"
		  				exit
				end
			end
		end
		[request(url: 'https://direct.yandex.ru/registered/main.pl', https: true, get: { cmd: 'showCompetitors', phrase: keyword }, current_session: true), nil]
	end

	def update_direct_cert
		key = request(url: 'https://direct.yandex.ru/registered/main.pl?cmd=certList&ulogin=', current_session: true).body.scan(/window.csrf_token = '(.*?)'/)[0][0]
		request url: "https://direct.yandex.ru/registered/main.#{key}.pl", post: { cmd: 'certCreate', type: 'create', days: 90, req_type: '' }, multipart: true, current_session: true
	end

	def try_login(acc = {})
		acc[:login] ||= @login
		acc[:password] ||= @password
		return false if acc[:login].nil? or acc[:password].nil?

		# https://passport.yandex.ru

		result = request post: { 'login' => acc[:login], 'passwd' => acc[:password], 'twoweeks' => 'yes', 'timestamp' => Time.now.to_i }, get: { mode: 'auth', retpath: 'https://direct.yandex.ru' }, https: true
		return nil, CONNECTION_ERROR if result.nil?
		custom_cookies = result.get_cookies

		return nil, LOGIN_ERROR unless result.code.to_i.eql? 302
		@session = custom_cookies.join '; '

		result = request url: result.location, https: true, current_session: true
		return nil, CONNECTION_ERROR if result.nil?

		result = request url: result.location, https: true, current_session: true
		return nil, CONNECTION_ERROR if result.nil?

		if(/^https:\/\/direct.yandex.ru(.*?)/ =~ result.location)
			@logged = true
		else
			return nil, LOGIN_ERROR
		end

		result = request url: 'https://direct.yandex.ru/registered/main.pl?cmd=ForecastByWords', https: true, current_session: true
		return nil, CONNECTION_ERROR if result.nil?

		if result.body.force_encoding("UTF-8").include? 'Контрольные цифры'
			return nil, CATPCHA_DETECTED
		end
		true
	end

	private

	def request(options = {})
		begin
			options[:url] ||=  'https://passport.yandex.ru/passport'
			options[:get] ||= {}
			options[:post] ||= false
			options[:https] ||= false
			options[:current_session] ||= nil
			options[:follow_location] ||= false
			options[:multipart] ||= nil

			# p options[:url]
			url = options[:url]

			unless options[:get].empty?
				url = options[:url] + '?' + URI.escape(options[:get].collect{|k,v| "#{k}=#{v}"}.join('&'))
			end

			c = Curl::Easy.new(url)
			c.headers["User-Agent"] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0'
			c.headers['Cookie'] = @session if options[:current_session]
			c.headers['Referer'] = 'http://www.yandex.ru/'
			c.headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0'
			c.headers['Connection'] = 'keep-alive'
			c.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
	      	c.headers['Accept-Language'] = 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3'
	      	c.timeout = 15

			unless @proxy.nil?
		      	c.proxy_url = @proxy
				c.proxy_type = @proxy_type
				c.proxypwd = @proxy_pw unless @proxy_pw.nil?
	      	end

			c.ssl_verify_peer = false
			c.ssl_verify_host = false

			# c.on_debug { |code, action|
			# 	p action
			# }

			c.verbose = false
			c.follow_location = options[:follow_location]
			c.enable_cookies = true

			c.multipart_form_post = true unless options[:multipart].nil? 

			if options[:post]
				post_data = options[:post].map { |k, v| Curl::PostField.content(k.to_s, v.to_s) }

				unless options[:file].nil?
					post_data << Curl::PostField.file('file', options[:file])
				end

				result = c.http_post(post_data)
			else
				result = c.http_get
			end
			return c if result
			nil
		rescue Exception => msg
			# p msg
			nil
		end
	end
end
